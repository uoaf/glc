#include "color.hpp"
#include "glcutil.hpp"
#include <string.h>

namespace glc {

// Color constants http://en.wikipedia.org/wiki/Web_colors

const glm::vec4 colorblack = { 0.0f, 0.0f, 0.0f, 1.0f };
const glm::vec4 colorwhite = { 1.0f, 1.0f, 1.0f, 1.0f };

const glm::vec4 colorlightgray = { (211.0f / 255.0f), (211.0f / 255.0f),
  (211.0f / 255.0f), 1.0f };
const glm::vec4 colorgray = { 0.5f, 0.5f, 0.5f, 1.0f };
const glm::vec4 colordarkgray = { (169.0f / 255.0f), (169.0f / 255.0f),
  (169.0f / 255.0f), 1.0f };

const glm::vec4 colorlightblue = { (173.0f / 255.0f), (216.0f / 255.0f),
  (230.0f / 255.0f), 1.0f };
const glm::vec4 colorblue = { 0.0f, 0.0f, 1.0f, 1.0f };
const glm::vec4 colordarkblue = { 0.0f, 0.0f, (139.0f / 255.0f), 1.0f };
const glm::vec4 colorlightgreen = { 0.0f, 1.0f, 0.0f, 1.0f };
const glm::vec4 colorgreen = { 0.0f, 0.5f, 0.0f, 1.0f };
const glm::vec4 colordarkgreen = { 0.0f, (100.0f / 255.0f), 0.0f, 1.0f };
const glm::vec4 colorlightred = { 1.0f, (192.0f / 255.0f), (203.0f / 255.0f), 1.0f };
const glm::vec4 colorred = { 1.0f, 0.0f, 0.0f, 1.0f };
const glm::vec4 colordarkred = { (139.0f / 255.0f), 0.0f, 0.0f, 1.0f };
const glm::vec4 colorlightyellow = { 1.0f, 1.0f, (224.0f / 255.0f), 1.0f };
const glm::vec4 coloryellow = { 1.0f, 1.0f, 0.0f, 1.0f };
const glm::vec4 colordarkyellow = { 1.0f, (215.0f / 255.0f), 0.0f, 1.0f };

const glm::vec4 colorlightorange = { 1.0f, (165.0f / 255.0f), 0.0f, 1.0f };
const glm::vec4 colororange = { 1.0f, (165.0f / 255.0f), 0.0f, 1.0f };
const glm::vec4 colordarkorange = { 1.0f, (140.0f / 255.0f), 0.0f, 1.0f };
const glm::vec4 colorlightpurple = { (221.0f / 255.0f), 0.0f, (221.0f / 255.0f),
  1.0f };
const glm::vec4 colorpurple = { 0.5f, 0.0f, 0.5f, 1.0f };
const glm::vec4 colordarkpurple = { (75.0f / 255.0f), 0.0f, (130.0f / 255.0f),
  1.0f };
const glm::vec4 colorlightbrown = { (210.0f / 255.0f), (180.0f / 255.0f),
  (140.0f / 255.0f), 1.0f };
const glm::vec4 colorbrown = { (165.0f / 255.0f), (42.0f / 255.0f),
  (42.0f / 255.0f), 1.0f };
const glm::vec4 colordarkbrown = { (210.0f / 255.0f), (105.0f / 255.0f),
  (30.0f / 255.0f), 1.0f };

const float colorhuered = (0.0f);
const float colorhueyellow = (60.0f);
const float colorhuegreen = (120.0f);
const float colorhuecyan = (180.0f);
const float colorhueblue = (240.0f);
const float colorhuepurple = (300.0f);

// Compare color.
bool colorEqual(const glm::vec4& lhs, const glm::vec4& rhs,
    const float epsilon /* = MATH_EPSILON */) {
  return (equal(lhs[0], rhs[0], epsilon) && equal(lhs[1], rhs[1], epsilon) && equal(lhs[2], rhs[2], epsilon) && equal(lhs[3], rhs[3], epsilon));
}
bool colorNotEqual(const glm::vec4& lhs, const glm::vec4& rhs,
    const float epsilon /* = MATH_EPSILON */) {
  return (notEqual(lhs[0], rhs[0], epsilon) || notEqual(lhs[1], rhs[1], epsilon) || notEqual(lhs[2], rhs[2], epsilon) || notEqual(lhs[3], rhs[3], epsilon));
}

// Set color components.
void colorSetColor(glm::vec4& result, float r, float g, float b, float a) {
  result[0] = r;
  result[1] = g;
  result[2] = b;
  result[3] = a;
}
void colorSetColor(glm::vec4& result, float& r, float& g, float& b) {
  result[0] = r;
  result[1] = g;
  result[2] = b;
  result[3] = 1.0f;
}

// Convert to 32-bit RGBA8888
uint32_t colorToPixelRGBA(const glm::vec4& color) {
  uint32_t r = color[0] * 255;
  uint32_t g = color[1] * 255;
  uint32_t b = color[2] * 255;
  uint32_t a = color[3] * 255;
  // Endian
  uint32_t result = ((a & 0xFF) << 24) | ((b & 0xFF) << 16) | ((g & 0xFF) << 8) | (r & 0xFF);
  return result;
}

// Convert from 32-bit RGBA8888
void colorFromPixelRGBA(glm::vec4& resultcolor, uint32_t pixel) {
  uint32_t r = (pixel >> 24) & 0xFF;
  uint32_t g = (pixel >> 16) & 0xFF;
  uint32_t b = (pixel >> 8) & 0xFF;
  uint32_t a = pixel & 0xFF;
  resultcolor[0] = r / 255.0f;
  resultcolor[1] = g / 255.0f;
  resultcolor[2] = b / 255.0f;
  resultcolor[3] = a / 255.0f;
}

// (1-r, 1-g, 1-b, a)
void colorNegative(glm::vec4& result, const glm::vec4& color) {
  result[0] = 1.0f - color[0];
  result[1] = 1.0f - color[1];
  result[2] = 1.0f - color[2];
  result[3] = color[3];
}

// Adds R,G,B,A componets of two colors.  Does not check if values are greater
// than 1.0f.
void colorAddOvershoot(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] + color2[0];
  result[1] = color1[1] + color2[1];
  result[2] = color1[2] + color2[2];
  result[3] = color1[3] + color2[3];
}

// Adds R,G,B,A componets of two colors. Clamp at 1.0f.
void colorAddClamp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2) {
  result[0] = color1[0] + color2[0];
  result[1] = color1[1] + color2[1];
  result[2] = color1[2] + color2[2];
  result[3] = color1[3] + color2[3];

  result[0] = clampHigh(result[0], 1.0f);
  result[1] = clampHigh(result[1], 1.0f);
  result[2] = clampHigh(result[2], 1.0f);
  result[3] = clampHigh(result[3], 1.0f);
}

// Adds R,G,B,A componets of two colors. If value is greater than 1.0f, the
// value wraps around starting from 0.0f.
void colorAddWrap(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2) {
  result[0] = color1[0] + color2[0];
  result[1] = color1[1] + color2[1];
  result[2] = color1[2] + color2[2];
  result[3] = color1[3] + color2[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// Subtract R,G,B,A componets of two colors. Does not check if values are less
// than 0.0f.
void colorSubtractOvershoot(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] - color2[0];
  result[1] = color1[1] - color2[1];
  result[2] = color1[2] - color2[2];
  result[3] = color1[3] - color2[3];
}

// Subtract R,G,B,A componets of two colors. Clamp at 0.0f.
void colorSubtractClamp(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] - color2[0];
  result[1] = color1[1] - color2[1];
  result[2] = color1[2] - color2[2];
  result[3] = color1[3] - color2[3];

  result[0] = clampLow(result[0], 0.0f);
  result[1] = clampLow(result[1], 0.0f);
  result[2] = clampLow(result[2], 0.0f);
  result[3] = clampLow(result[3], 0.0f);
}

// Adds R,G,B,A componets. If value is less than 0.0f, the value wraps around
// starting from 1.0f.
void colorSubtractWrap(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] - color2[0];
  result[1] = color1[1] - color2[1];
  result[2] = color1[2] - color2[2];
  result[3] = color1[3] - color2[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// Scale the R,G,B,A componets of a color. Does not check if values are out of
// the range 0.0f to 1.0f.
void colorScaleOvershoot(glm::vec4& result, const glm::vec4& color, float s) {
  result[0] = s * color[0];
  result[1] = s * color[1];
  result[2] = s * color[2];
  result[3] = s * color[3];
}

// Scale the R,G,B,A componets of a color. Clamps values to the range 0.0f
// to 1.0f.
void colorScaleClamp(glm::vec4& result, const glm::vec4& color, float s) {
  result[0] = s * color[0];
  result[1] = s * color[1];
  result[2] = s * color[2];
  result[3] = s * color[3];

  result[0] = clampUnit(result[0]);
  result[1] = clampUnit(result[1]);
  result[2] = clampUnit(result[2]);
  result[3] = clampUnit(result[3]);
}

// Scale the R,G,B,A componets of a color. If value is less than 0.0f, the value
// wraps around starting from 1.0f.
void colorScaleWrap(glm::vec4& result, const glm::vec4& color, float s) {
  result[0] = s * color[0];
  result[1] = s * color[1];
  result[2] = s * color[2];
  result[3] = s * color[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// (r1*r2, g1*g2, b1*b2, a1*a2)
void colorModulateOvershoot(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] * color2[0];
  result[1] = color1[1] * color2[1];
  result[2] = color1[2] * color2[2];
  result[3] = color1[3] * color2[3];
}

// (r1*r2, g1*g2, b1*b2, a1*a2)  Clamps values to the range 0.0f to 1.0f.
void colorModulateClamp(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] * color2[0];
  result[1] = color1[1] * color2[1];
  result[2] = color1[2] * color2[2];
  result[3] = color1[3] * color2[3];

  result[0] = glm::clamp(result[0], 0.0f, 1.0f);
  result[1] = glm::clamp(result[1], 0.0f, 1.0f);
  result[2] = glm::clamp(result[2], 0.0f, 1.0f);
  result[3] = glm::clamp(result[3], 0.0f, 1.0f);
}

// (r1*r2, g1*g2, b1*b2, a1*a2)  If value is less than 0.0f, the value wraps
// around starting from 1.0f.
void colorModulateWrap(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2) {
  result[0] = color1[0] * color2[0];
  result[1] = color1[1] * color2[1];
  result[2] = color1[2] * color2[2];
  result[3] = color1[3] * color2[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// Scale and Add  R,G,B,A  componets.  Does not check if values are greater
// than 1.0f. result = (c1 * s) + c2
void colorScaleAddOvershoot(glm::vec4& result, const glm::vec4& color1, float s,
    const glm::vec4& color2) {
  result[0] = (s * color1[0]) + color2[0];
  result[1] = (s * color1[1]) + color2[1];
  result[2] = (s * color1[2]) + color2[2];
  result[3] = (s * color1[3]) + color2[3];
}

// Scale and Add  R,G,B,A  componets.  Clamp at 1.0f.
// result = (c1 * s) + c2
void colorScaleAddClamp(glm::vec4& result, const glm::vec4& color1, float s,
    const glm::vec4& color2) {
  result[0] = (s * color1[0]) + color2[0];
  result[1] = (s * color1[1]) + color2[1];
  result[2] = (s * color1[2]) + color2[2];
  result[3] = (s * color1[3]) + color2[3];

  result[0] = glm::clamp(result[0], 0.0f, 1.0f);
  result[1] = glm::clamp(result[1], 0.0f, 1.0f);
  result[2] = glm::clamp(result[2], 0.0f, 1.0f);
  result[3] = glm::clamp(result[3], 0.0f, 1.0f);
}

// Scale and Add  R,G,B,A  componets.  If value is greater than 1.0f, the value
// wraps around starting from 0.0f. result = (c1 * s) + c2
void colorScaleAddWrap(glm::vec4& result, const glm::vec4& color1, float s,
    const glm::vec4& color2) {
  result[0] = (s * color1[0]) + color2[0];
  result[1] = (s * color1[1]) + color2[1];
  result[2] = (s * color1[2]) + color2[2];
  result[3] = (s * color1[3]) + color2[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// Modulate and Add  R,G,B,A  componets.  Does not check if values are greater
// than 1.0f. result = (c1 * c2) + c3
void colorModulateAddOvershoot(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2, const glm::vec4& color3) {
  result[0] = (color1[0] * color2[0]) + color3[0];
  result[1] = (color1[1] * color2[1]) + color3[1];
  result[2] = (color1[2] * color2[2]) + color3[2];
  result[3] = (color1[3] * color2[3]) + color3[3];
}

// Modulate and Add  R,G,B,A  componets.  Clamp at 1.0f.
// result = (c1 * c2) + c3
void colorModulateAddClamp(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2, const glm::vec4& color3) {
  result[0] = (color1[0] * color2[0]) + color3[0];
  result[1] = (color1[1] * color2[1]) + color3[1];
  result[2] = (color1[2] * color2[2]) + color3[2];
  result[3] = (color1[3] * color2[3]) + color3[3];

  result[0] = glm::clamp(result[0], 0.0f, 1.0f);
  result[1] = glm::clamp(result[1], 0.0f, 1.0f);
  result[2] = glm::clamp(result[2], 0.0f, 1.0f);
  result[3] = glm::clamp(result[3], 0.0f, 1.0f);
}

// Modulate and Add  R,G,B,A  componets.  If value is greater than 1.0f, the
// value wraps around starting from 0.0f. result = (c1 * c2) + c3
void colorModulateAddWrap(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2, const glm::vec4& color3) {
  result[0] = (color1[0] * color2[0]) + color3[0];
  result[1] = (color1[1] * color2[1]) + color3[1];
  result[2] = (color1[2] * color2[2]) + color3[2];
  result[3] = (color1[3] * color2[3]) + color3[3];

  result[0] = wrapUnit(result[0]);
  result[1] = wrapUnit(result[1]);
  result[2] = wrapUnit(result[2]);
  result[3] = wrapUnit(result[3]);
}

// Linear interpolation of r,g,b, and a. color1 + s(color2-color1)
void colorLerp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2,
    float s) {
  result[0] = lerp(color1[0], color2[0], s);
  result[1] = lerp(color1[1], color2[1], s);
  result[2] = lerp(color1[2], color2[2], s);
  result[3] = lerp(color1[3], color2[3], s);
}

// Smooth 'ess-shape' or 'S-shape' interpolation of R,G,B,A components.
// Hermite interpolation between position 1 (when s == 0) and position 2 (when s
// == 1), with implicit tangents that run parallel to the time axis (horizontal
// time line).
void colorHermiteSansTangent(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2, float s) {
  float ss = s * s; // s squared
  float sss = ss * s; // s cubed
  float h1 = 2.0f * sss - 3.0f * ss + 1.0f; // basis function 1
  float h2 = -2.0f * sss + 3.0f * ss; // basis function 2

  result[0] = (h1 * color1[0]) + (h2 * color2[0]);
  result[1] = (h1 * color1[1]) + (h2 * color2[1]);
  result[2] = (h1 * color1[2]) + (h2 * color2[2]);
  result[3] = (h1 * color1[3]) + (h2 * color2[3]);
}

// HSL colorspace interpolate colors.
// Convert RBG to HSV, interpolate HSV components and Alpha, convert result HSV
// to RGB.
void colorHSVLerp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2,
    float s) {
  float h1, s1, v1, h2, s2, v2;
  colorRGBtoHSV(h1, s1, v1, color1[0], color1[1], color1[2]);
  colorRGBtoHSV(h2, s2, v2, color2[0], color2[1], color2[2]);

  float hresult = lerp(h1, h2, s);
  float sresult = lerp(s1, s2, s);
  float vresult = lerp(v1, v2, s);
  result[3] = lerp(color1[3], color2[3], s);

  colorHSVtoRGB(result[0], result[1], result[2], hresult, sresult, vresult);
}

// HSL colorspace interpolate colors.
// Convert RBG to HSV, interpolate HSV components and Alpha, convert result HSV
// to RGB.
void colorHSVHermiteSansTangent(glm::vec4& result, const glm::vec4& color1,
    const glm::vec4& color2, float s) {
  float hue1, s1, v1, hue2, s2, v2;
  colorRGBtoHSV(hue1, s1, v1, color1[0], color1[1], color1[2]);
  colorRGBtoHSV(hue2, s2, v2, color2[0], color2[1], color2[2]);

  float ss = s * s; // s squared
  float sss = ss * s; // s cubed
  float h1 = 2.0f * sss - 3.0f * ss + 1.0f; // basis function 1
  float h2 = -2.0f * sss + 3.0f * ss; // basis function 2

  float hresult = (h1 * hue1) + (h2 * hue2);
  float sresult = (h1 * s1) + (h2 * s2);
  float vresult = (h1 * v1) + (h2 * v2);
  result[3] = (h1 * color1[3]) + (h2 * color2[3]);

  colorHSVtoRGB(result[0], result[1], result[2], hresult, sresult, vresult);
}

// Interpolate r,g,b between desaturated color and color.
// DesaturatedColor + s(Color - DesaturatedColor)
void colorAdjustSaturation(glm::vec4& result, const glm::vec4& color, float s) {
  float hue, saturation, value;
  colorRGBtoHSV(hue, saturation, value, color[0], color[1], color[2]);

  s = glm::clamp(s, 0.0f, 1.0f);
  colorHSVtoRGB(result[0], result[1], result[2], hue, s, value);
  result[3] = color[3];
}

// Interpolate r,g,b between 50% grey and color.  Grey + s(Color - Grey)
void colorAdjustContrast(glm::vec4& result, const glm::vec4& color, float c) {
  float hue, saturation, value;
  colorRGBtoHSV(hue, saturation, value, color[0], color[1], color[2]);

  c = glm::clamp(c, 0.0f, 1.0f);
  colorHSVtoRGB(result[0], result[1], result[2], hue, saturation, c);
  result[3] = color[3];
}

// See also:
// http://en.wikipedia.org/wiki/HSV_color_space

// Color space conversion code has multiple different
// sources all over the internet.  Here are a few...
//	http://www.cs.bham.ac.uk/~mer/colour/hsv.html
//	http://www.codeproject.com/miscctrl/CPicker.asp?df=100&forumid=141207&exp=0&select=1144309
//	http://www.google.com/search?hl=en&lr=&q=HSVtoRGB+C%2B%2B
//	http://www.vbforums.com/archive/index.php/t-340926.html
//	http://www.koders.com/java/fidD6E751EC32DB792C9748C78448E9C012D03C2F00.aspx

// HSL colorspace conversion
// Convert to Red, Green, Blue from Hue, Saturation, Value.
//  Red, Green, Blue are normalized between 0 and 1.
//  Hue is an angle between 0 and 360.
//  Saturation is between 0 and 1, 0 = no saturation (or white), 1 = completly
//  saturated. Value is between 0 and 1, 0 = no brightness (or black), 1 =
//  bright.
void colorHSVtoRGB(float& r, float& g, float& b, float h, float s, float v) {
  int i;
  float f, p, q, t, htemp;

  // s==0? unsaturated = grey so R,G and B all equal value
  if(s == 0.0 || h == -1.0) {
    r = g = b = v;
    return;
  }

  htemp = h / 60.0f;
  i = (int)floor(htemp); // which sector
  f = htemp - i; // how far through sector
  p = v * (1 - s);
  q = v * (1 - s * f);
  t = v * (1 - s * (1 - f));

  switch(i) {
  case 0:
    r = v;
    g = t;
    b = p;
    break;
  case 1:
    r = q;
    g = v;
    b = p;
    break;
  case 2:
    r = p;
    g = v;
    b = t;
    break;
  case 3:
    r = p;
    g = q;
    b = v;
    break;
  case 4:
    r = t;
    g = p;
    b = v;
    break;
  case 5:
    r = v;
    g = p;
    b = q;
    break;
  }
}

// Convert to Hue, Saturation, Value from Red, Green Blue.
//  Red, Green, Blue are normalized between 0 and 1.
//  Hue is an angle between 0 and 360.
//  Saturation is between 0 and 1, 0 = no saturation (or white), 1 = completly
//  saturated. Value is between 0 and 1, 0 = no brightness (or black), 1 =
//  bright.
void colorRGBtoHSV(float& h, float& s, float& v, float r, float g, float b) {
  float mn = r;
  float mx = r;
  int maxVal = 0;

  if(g > mx) {
    mx = g;
    maxVal = 1;
  }
  if(b > mx) {
    mx = b;
    maxVal = 2;
  }
  if(g < mn)
    mn = g;
  if(b < mn)
    mn = b;

  float delta = mx - mn;
  v = mx;

  if(mx != 0) {
    s = delta / mx;
  } else {
    s = 0;
    h = 0;
    return;
  }
  if(s == 0.0f) {
    h = -1;
    return;
  } else {
    switch(maxVal) {
    case 0:
      h = (g - b) / delta; // yel < h < mag
      break;
    case 1:
      h = 2 + (b - r) / delta; // cyan < h < yel
      break;
    case 2:
      h = 4 + (r - g) / delta; // mag < h < cyan
      break;
    }
  }
  h *= 60;
  if(h < 0)
    h += 360;
}

// color constants   http://en.wikipedia.org/wiki/Web_colors
// void colorToStringName( char* szColorName, const glm::vec4& color );
// void colorFromStringName( glm::vec4& color, const char* szColorName );

/*
Red colors
IndianRed		CD 5C 5C	205  92  92
LightCoral		F0 80 80	240 128 128
Salmon			FA 80 72	250 128 114
DarkSalmon		E9 96 7A	233 150 122
LightSalmon		FF A0 7A	255 160 122
Crimson			DC 14 3C	220  20  60
Red				FF 00 00	255   0   0
FireBrick		B2 22 22	178  34  34
DarkRed			8B 00 00	139   0   0



Pink colors
Pink			FF C0 CB	255 192 203
LightPink		FF B6 C1	255 182 193
HotPink			FF 69 B4	255 105 180
DeepPink		FF 14 93	255  20 147
MediumVioletRed	C7 15 85	199  21 133
PaleVioletRed	DB 70 93	219 112 147


Orange colors
LightSalmon		FF A0 7A	255 160 122
Coral			FF 7F 50	255 127  80
Tomato			FF 63 47	255  99  71
OrangeRed		FF 45 00	255  69   0
DarkOrange		FF 8C 00	255 140   0
Orange			FF A5 00	255 165   0


Yellow colors
Gold			FF D7 00	255 215   0
Yellow			FF FF 00	255 255   0
LightYellow		FF FF E0	255 255 224
LemonChiffon	FF FA CD	255 250 205
LightGoldenrodYellow	FA FA D2	250 250 210
PapayaWhip		FF EF D5	255 239 213
Moccasin		FF E4 B5	255 228 181
PeachPuff		FF DA B9	255 218 185
PaleGoldenrod	EE E8 AA	238 232 170
Khaki			F0 E6 8C	240 230 140
DarkKhaki		BD B7 6B	189 183 107


Purple colors
Lavender		E6 E6 FA	230 230 250
Thistle			D8 BF D8	216 191 216
Plum			DD A0 DD	221 160 221
Violet			EE 82 EE	238 130 238
Orchid			DA 70 D6	218 112 214
Fuchsia			FF 00 FF	255   0 255
Magenta			FF 00 FF	255   0 255
MediumOrchid	BA 55 D3	186  85 211
MediumPurple	93 70 DB	147 112 219
Amethyst		99 66 CC	153 102 204
BlueViolet		8A 2B E2	138  43 226
DarkViolet		94 00 D3	148   0 211
DarkOrchid		99 32 CC	153  50 204
DarkMagenta		8B 00 8B	139   0 139
Purple			80 00 80	128   0 128
Indigo			4B 00 82	 75   0 130
SlateBlue		6A 5A CD	106  90 205
DarkSlateBlue	48 3D 8B	 72  61 139
MediumSlateBlue	7B 68 EE	123 104 238


Green colors
GreenYellow		AD FF 2F	173 255  47
Chartreuse		7F FF 00	127 255   0
LawnGreen		7C FC 00	124 252   0
Lime			00 FF 00	  0 255   0
LimeGreen		32 CD 32	 50 205  50
PaleGreen		98 FB 98	152 251 152
LightGreen		90 EE 90	144 238 144
MediumSpringGreen	00 FA 9A	  0 250 154
SpringGreen		00 FF 7F	  0 255 127
MediumSeaGreen	3C B3 71	 60 179 113
SeaGreen		2E 8B 57	 46 139  87
ForestGreen		22 8B 22	 34 139  34
Green			00 80 00	  0 128   0
DarkGreen		00 64 00	  0 100   0
YellowGreen		9A CD 32	154 205  50
OliveDrab		6B 8E 23	107 142  35
Olive			80 80 00	128 128   0
DarkOliveGreen	55 6B 2F	 85 107  47
MediumAquamarine	66 CD AA	102 205 170
DarkSeaGreen	8F BC 8F	143 188 143
LightSeaGreen	20 B2 AA	 32 178 170
DarkCyan		00 8B 8B	  0 139 139
Teal			00 80 80	  0 128 128


Blue colors
Aqua			00 FF FF	  0 255 255
Cyan			00 FF FF	  0 255 255
LightCyan		E0 FF FF	224 255 255
PaleTurquoise	AF EE EE	175 238 238
Aquamarine		7F FF D4	127 255 212
Turquoise		40 E0 D0	 64 224 208
MediumTurquoise	48 D1 CC	 72 209 204
DarkTurquoise	00 CE D1	  0 206 209
CadetBlue		5F 9E A0	 95 158 160
SteelBlue		46 82 B4	 70 130 180
LightSteelBlue	B0 C4 DE	176 196 222
PowderBlue		B0 E0 E6	176 224 230
LightBlue		AD D8 E6	173 216 230
SkyBlue			87 CE EB	135 206 235
LightSkyBlue	87 CE FA	135 206 250
DeepSkyBlue		00 BF FF	  0 191 255
DodgerBlue		1E 90 FF	 30 144 255
CornflowerBlue	64 95 ED	100 149 237
MediumSlateBlue	7B 68 EE	123 104 238
RoyalBlue		41 69 E1	 65 105 225
Blue			00 00 FF	  0   0 255
MediumBlue		00 00 CD	  0   0 205
DarkBlue		00 00 8B	  0   0 139
Navy			00 00 80	  0   0 128
MidnightBlue	19 19 70	 25  25 112


Brown colors
Cornsilk		FF F8 DC	255 248 220
BlanchedAlmond	FF EB CD	255 235 205
Bisque			FF E4 C4	255 228 196
NavajoWhite		FF DE AD	255 222 173
Wheat			F5 DE B3	245 222 179
BurlyWood		DE B8 87	222 184 135
Tan				D2 B4 8C	210 180 140
RosyBrown		BC 8F 8F	188 143 143
SandyBrown		F4 A4 60	244 164  96
Goldenrod		DA A5 20	218 165  32
DarkGoldenrod	B8 86 0B	184 134  11
Peru			CD 85 3F	205 133  63
Chocolate		D2 69 1E	210 105  30
SaddleBrown		8B 45 13	139  69  19
Sienna			A0 52 2D	160  82  45
Brown			A5 2A 2A	165  42  42
Maroon			80 00 00	128   0   0


White colors
White			FF FF FF	255 255 255
Snow			FF FA FA	255 250 250
Honeydew		F0 FF F0	240 255 240
MintCream		F5 FF FA	245 255 250
Azure			F0 FF FF	240 255 255
AliceBlue		F0 F8 FF	240 248 255
GhostWhite		F8 F8 FF	248 248 255
WhiteSmoke		F5 F5 F5	245 245 245
Seashell		FF F5 EE	255 245 238
Beige			F5 F5 DC	245 245 220
OldLace			FD F5 E6	253 245 230
FloralWhite		FF FA F0	255 250 240
Ivory			FF FF F0	255 255 240
AntiqueWhite	FA EB D7	250 235 215
Linen			FA F0 E6	250 240 230
LavenderBlush	FF F0 F5	255 240 245
MistyRose		FF E4 E1	255 228 225


Grey colors
Gainsboro		DC DC DC	220 220 220
LightGrey		D3 D3 D3	211 211 211
Silver			C0 C0 C0	192 192 192
DarkGray		A9 A9 A9	169 169 169
Gray			80 80 80	128 128 128
DimGray			69 69 69	105 105 105
LightSlateGray	77 88 99	119 136 153
SlateGray		70 80 90	112 128 144
DarkSlateGray	2F 4F 4F	 47  79  79
Black			00 00 00	  0   0   0
*/

struct named_color_t {
  const char* szName;
  unsigned int color;
};

#define NAMED_RGB(r, g, b) \
  (((unsigned int)r) | ((unsigned int)g << 8) | ((unsigned int)b << 16))

static named_color_t _named_colors[] = {
  // a
  { "aliceblue", NAMED_RGB(240, 248, 255) },
  { "antiquewhite", NAMED_RGB(250, 235, 215) },
  { "aqua", NAMED_RGB(0, 255, 255) },
  { "aquamarine", NAMED_RGB(127, 255, 212) },
  { "azure", NAMED_RGB(240, 255, 255) },
  // b
  { "beige", NAMED_RGB(245, 245, 220) },
  { "bisque", NAMED_RGB(255, 228, 196) },
  { "black", NAMED_RGB(0, 0, 0) },
  { "blanchedalmond", NAMED_RGB(255, 235, 205) },
  { "blue", NAMED_RGB(0, 0, 255) },
  { "blueviolet", NAMED_RGB(138, 43, 226) },
  { "brown", NAMED_RGB(165, 42, 42) },
  { "burlywood", NAMED_RGB(222, 184, 135) },
  // c
  { "cadetblue", NAMED_RGB(95, 158, 160) },
  { "chartreuse", NAMED_RGB(127, 255, 0) },
  { "chocolate", NAMED_RGB(210, 105, 30) },
  { "coral", NAMED_RGB(255, 127, 80) },
  { "cornflowerblue", NAMED_RGB(100, 149, 237) },
  { "cornsilk", NAMED_RGB(255, 248, 220) },
  { "crimson", NAMED_RGB(220, 20, 60) },
  { "cyan", NAMED_RGB(0, 255, 255) },
  // d
  { "darkblue", NAMED_RGB(0, 0, 139) },
  { "darkcyan", NAMED_RGB(0, 139, 139) },
  { "darkgoldenrod", NAMED_RGB(184, 134, 11) },
  { "darkgray", NAMED_RGB(169, 169, 169) },
  { "darkgreen", NAMED_RGB(0, 100, 0) },
  { "darkgrey", NAMED_RGB(169, 169, 169) },
  { "darkkhaki", NAMED_RGB(189, 183, 107) },
  { "darkmagenta", NAMED_RGB(139, 0, 139) },
  { "darkolivegreen", NAMED_RGB(85, 107, 47) },
  { "darkorange", NAMED_RGB(255, 140, 0) },
  { "darkorchid", NAMED_RGB(153, 50, 204) },
  { "darkred", NAMED_RGB(139, 0, 0) },
  { "darksalmon", NAMED_RGB(233, 150, 122) },
  { "darkseagreen", NAMED_RGB(143, 188, 143) },
  { "darkslateblue", NAMED_RGB(72, 61, 139) },
  { "darkslategray", NAMED_RGB(47, 79, 79) },
  { "darkslategrey", NAMED_RGB(47, 79, 79) },
  { "darkturquoise", NAMED_RGB(0, 206, 209) },
  { "darkviolet", NAMED_RGB(148, 0, 211) },
  { "deeppink", NAMED_RGB(255, 20, 147) },
  { "deepskyblue", NAMED_RGB(0, 191, 255) },
  { "dimgray", NAMED_RGB(105, 105, 105) },
  { "dimgrey", NAMED_RGB(105, 105, 105) },
  { "dodgerblue", NAMED_RGB(30, 144, 255) },
  // e - f
  { "firebrick", NAMED_RGB(178, 34, 34) },
  { "floralwhite", NAMED_RGB(255, 250, 240) },
  { "forestgreen", NAMED_RGB(34, 139, 34) },
  { "fuchsia", NAMED_RGB(255, 0, 255) },
  // g
  { "gainsboro", NAMED_RGB(220, 220, 220) },
  { "ghostwhite", NAMED_RGB(248, 248, 255) },
  { "gold", NAMED_RGB(255, 215, 0) },
  { "goldenrod", NAMED_RGB(218, 165, 32) },
  { "gray", NAMED_RGB(128, 128, 128) },
  { "green", NAMED_RGB(0, 128, 0) },
  { "greenyellow", NAMED_RGB(173, 255, 47) },
  { "grey", NAMED_RGB(128, 128, 128) },
  // h - i - j - k
  { "honeydew", NAMED_RGB(240, 255, 240) },
  { "hotpink", NAMED_RGB(255, 105, 180) },
  { "indianred", NAMED_RGB(205, 92, 92) },
  { "indigo", NAMED_RGB(75, 0, 130) },
  { "ivory", NAMED_RGB(255, 255, 240) },
  { "khaki", NAMED_RGB(240, 230, 140) },
  // l
  { "lavender", NAMED_RGB(230, 230, 250) },
  { "lavenderblush", NAMED_RGB(255, 240, 245) },
  { "lawngreen", NAMED_RGB(124, 252, 0) },
  { "lemonchiffon", NAMED_RGB(255, 250, 205) },
  { "lightblue", NAMED_RGB(173, 216, 230) },
  { "lightcoral", NAMED_RGB(240, 128, 128) },
  { "lightcyan", NAMED_RGB(224, 255, 255) },
  { "lightgoldenrodyellow", NAMED_RGB(250, 250, 210) },
  { "lightgray", NAMED_RGB(211, 211, 211) },
  { "lightgreen", NAMED_RGB(144, 238, 144) },
  { "lightgrey", NAMED_RGB(211, 211, 211) },
  { "lightpink", NAMED_RGB(255, 182, 193) },
  { "lightsalmon", NAMED_RGB(255, 160, 122) },
  { "lightseagreen", NAMED_RGB(32, 178, 170) },
  { "lightskyblue", NAMED_RGB(135, 206, 250) },
  { "lightslategray", NAMED_RGB(119, 136, 153) },
  { "lightslategrey", NAMED_RGB(119, 136, 153) },
  { "lightsteelblue", NAMED_RGB(176, 196, 222) },
  { "lightyellow", NAMED_RGB(255, 255, 224) },
  { "lime", NAMED_RGB(0, 255, 0) },
  { "limegreen", NAMED_RGB(50, 205, 50) },
  { "linen", NAMED_RGB(250, 240, 230) },
  // m
  { "maroon", NAMED_RGB(128, 0, 0) },
  { "magenta", NAMED_RGB(255, 0, 255) },
  { "mediumaquamarine", NAMED_RGB(102, 205, 170) },
  { "mediumblue", NAMED_RGB(0, 0, 205) },
  { "mediumorchid", NAMED_RGB(186, 85, 211) },
  { "mediumpurple", NAMED_RGB(147, 112, 219) },
  { "mediumseagreen", NAMED_RGB(60, 179, 113) },
  { "mediumslateblue", NAMED_RGB(123, 104, 238) },
  { "mediumspringgreen", NAMED_RGB(0, 250, 154) },
  { "mediumturquoise", NAMED_RGB(72, 209, 204) },
  { "mediumvioletred", NAMED_RGB(199, 21, 133) },
  { "midnightblue", NAMED_RGB(25, 25, 112) },
  { "mintcream", NAMED_RGB(245, 255, 250) },
  { "mistyrose", NAMED_RGB(255, 228, 225) },
  { "moccasin", NAMED_RGB(255, 228, 181) },
  // n
  { "navajowhite", NAMED_RGB(255, 222, 173) },
  { "navy", NAMED_RGB(0, 0, 128) },
  // o
  { "oldlace", NAMED_RGB(253, 245, 230) },
  { "olive", NAMED_RGB(128, 128, 0) },
  { "olivedrab", NAMED_RGB(107, 142, 35) },
  { "orange", NAMED_RGB(255, 165, 0) },
  { "orangered", NAMED_RGB(255, 69, 0) },
  { "orchid", NAMED_RGB(218, 112, 214) },
  // p
  { "palegoldenrod", NAMED_RGB(238, 232, 170) },
  { "palegreen", NAMED_RGB(152, 251, 152) },
  { "paleturquoise", NAMED_RGB(175, 238, 238) },
  { "palevioletred", NAMED_RGB(219, 112, 147) },
  { "papayawhip", NAMED_RGB(255, 239, 213) },
  { "peachpuff", NAMED_RGB(255, 218, 185) },
  { "peru", NAMED_RGB(205, 133, 63) },
  { "pink", NAMED_RGB(255, 192, 203) },
  { "plum", NAMED_RGB(221, 160, 221) },
  { "powderblue", NAMED_RGB(176, 224, 230) },
  { "purple", NAMED_RGB(128, 0, 128) },
  // q - r
  { "red", NAMED_RGB(255, 0, 0) },
  { "rosybrown", NAMED_RGB(188, 143, 143) },
  { "royalblue", NAMED_RGB(65, 105, 225) },
  // s
  { "saddlebrown", NAMED_RGB(139, 69, 19) },
  { "salmon", NAMED_RGB(250, 128, 114) },
  { "sandybrown", NAMED_RGB(244, 164, 96) },
  { "seagreen", NAMED_RGB(46, 139, 87) },
  { "seashell", NAMED_RGB(255, 245, 238) },
  { "sienna", NAMED_RGB(160, 82, 45) },
  { "silver", NAMED_RGB(192, 192, 192) },
  { "skyblue", NAMED_RGB(135, 206, 235) },
  { "slateblue", NAMED_RGB(106, 90, 205) },
  { "slategray", NAMED_RGB(112, 128, 144) },
  { "slategrey", NAMED_RGB(112, 128, 144) },
  { "snow", NAMED_RGB(255, 250, 250) },
  { "springgreen", NAMED_RGB(0, 255, 127) },
  { "steelblue", NAMED_RGB(70, 130, 180) },
  // t
  { "tan", NAMED_RGB(210, 180, 140) },
  { "teal", NAMED_RGB(0, 128, 128) },
  { "thistle", NAMED_RGB(216, 191, 216) },
  { "tomato", NAMED_RGB(255, 99, 71) },
  { "turquoise", NAMED_RGB(64, 224, 208) },
  // u - v - w - x - y - z
  { "violet", NAMED_RGB(238, 130, 238) },
  { "wheat", NAMED_RGB(245, 222, 179) },
  { "white", NAMED_RGB(255, 255, 255) },
  { "whitesmoke", NAMED_RGB(245, 245, 245) },
  { "yellow", NAMED_RGB(255, 255, 0) },
  { "yellowgreen", NAMED_RGB(154, 205, 50) },
};

// Returns index of item x, if x exists in the array. Returns -1 if item x does
// not exsit.
static int32_t binarysearch_namedcolor(const char* szName, named_color_t* array,
    int32_t n) {
  ASSERT(array);
  int32_t low, high, mid;
  low = -1;
  high = n;
  while(low + 1 < high) {
    mid = (low + high) / 2;
    if(strcasecmp(array[mid].szName, szName) < 0)
      low = mid;
    else
      high = mid;
  }
  if(high == n || (0 != strcasecmp(array[high].szName, szName)))
    return -1;
  else
    return high;
}

// Get color from string name. e.g. "red",  "lavender", "cadetblue"
// Returns: true if name was found, otherwise returns false
bool colorFromName(glm::vec4& result, const char* szName) {
  bool bSuccess = false;
  const int32_t n = sizeof(_named_colors) / sizeof(named_color_t);
  int32_t index = binarysearch_namedcolor(szName, _named_colors, n);
  if(index != -1) {
    bSuccess = true;
    uint32_t col = _named_colors[index].color;
    uint32_t r = col & 0xFF;
    uint32_t g = (col >> 8) & 0xFF;
    uint32_t b = (col >> 16) & 0xFF;
    result[0] = r / 255.0f;
    result[1] = g / 255.0f;
    result[2] = b / 255.0f;
    result[3] = 1.0f;
  }
  return bSuccess;
}

} // namespace glc
