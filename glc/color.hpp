#pragma once
#include "glminc.hpp"

namespace glc {

// Color constants http://en.wikipedia.org/wiki/Web_colors

extern const glm::vec4 colorblack;
extern const glm::vec4 colorwhite;

extern const glm::vec4 colorlightgray;
extern const glm::vec4 colorgray;
extern const glm::vec4 colordarkgray;

extern const glm::vec4 colorlightblue;
extern const glm::vec4 colorblue;
extern const glm::vec4 colordarkblue;
extern const glm::vec4 colorlightgreen;
extern const glm::vec4 colorgreen;
extern const glm::vec4 colordarkgreen;
extern const glm::vec4 colorlightred;
extern const glm::vec4 colorred;
extern const glm::vec4 colordarkred;
extern const glm::vec4 colorlightyellow;
extern const glm::vec4 coloryellow;
extern const glm::vec4 colordarkyellow;

extern const glm::vec4 colorlightorange;
extern const glm::vec4 colororange;
extern const glm::vec4 colordarkorange;
extern const glm::vec4 colorlightpurple;
extern const glm::vec4 colorpurple;
extern const glm::vec4 colordarkpurple;
extern const glm::vec4 colorlightbrown;
extern const glm::vec4 colorbrown;
extern const glm::vec4 colordarkbrown;

//  Hue is an angle between 0 and 360.
extern const float colorhuered;
extern const float colorhueyellow;
extern const float colorhuegreen;
extern const float colorhuecyan;
extern const float colorhueblue;
extern const float colorhuepurple;

// Compare color.
bool colorEqual(const glm::vec4& lhs, const glm::vec4& rhs, const float epsilon = glm::epsilon<float>());
bool colorNotEqual(const glm::vec4& lhs, const glm::vec4& rhs, const float epsilon = glm::epsilon<float>());

// Set color components.
void colorSetColor(glm::vec4& result, float r, float g, float b, float a);
void colorSetColor(glm::vec4& result, float r, float g, float b);

// Convert to 32-bit RGBA8888
uint32_t colorToPixelRGBA(const glm::vec4& color);

// Convert from 32-bit RGBA8888
void colorFromPixelRGBA(glm::vec4& resultcolor, uint32_t pixel);

// Get color from string name. e.g. "red", "lavender", "cadetblue"
// Returns: true if name was found, otherwise returns false
bool colorFromName(glm::vec4& result, const char* szName);

// (1-r, 1-g, 1-b, a)
void colorNegative(glm::vec4& result, const glm::vec4& color);

// Adds R,G,B,A componets of two colors.  Does not check if values are greater than 1.0f.
void colorAddOvershoot(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// Adds R,G,B,A componets of two colors. Clamp at 1.0f.
void colorAddClamp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// Adds R,G,B,A componets of two colors. If value is greater than 1.0f, the value wraps around starting from 0.0f.
void colorAddWrap(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);

// Subtract R,G,B,A componets of two colors. Does not check if values are less than 0.0f.
void colorSubtractOvershoot(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// Subtract R,G,B,A componets of two colors. Clamp at 0.0f.
void colorSubtractClamp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// Adds R,G,B,A componets. If value is less than 0.0f, the value wraps around starting from 1.0f.
void colorSubtractWrap(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);

// Scale the R,G,B,A componets of a color. Does not check if values are out of the range 0.0f to 1.0f.
void colorScaleOvershoot(glm::vec4& result, const glm::vec4& color, float s);
// Scale the R,G,B,A componets of a color. Clamps values to the range 0.0f to 1.0f.
void colorScaleClamp(glm::vec4& result, const glm::vec4& color, float s);
// Scale the R,G,B,A componets of a color. If value is less than 0.0f, the value wraps around starting from 1.0f.
void colorScaleWrap(glm::vec4& result, const glm::vec4& color, float s);

// (r1*r2, g1*g2, b1*b2, a1*a2)
void colorModulateOvershoot(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// (r1*r2, g1*g2, b1*b2, a1*a2)  Clamps values to the range 0.0f to 1.0f.
void colorModulateClamp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);
// (r1*r2, g1*g2, b1*b2, a1*a2)  If value is less than 0.0f, the value wraps around starting from 1.0f.
void colorModulateWrap(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2);

// Scale and Add  R,G,B,A  componets.  Does not check if values are greater than 1.0f.
// result = (c1 * s) + c2
void colorScaleAddOvershoot(glm::vec4& result, const glm::vec4& color1, float s, const glm::vec4& color2);
// Scale and Add  R,G,B,A  componets.  Clamp at 1.0f.
// result = (c1 * s) + c2
void colorScaleAddClamp(glm::vec4& result, const glm::vec4& color1, float s, const glm::vec4& color2);
// Scale and Add  R,G,B,A  componets.  If value is greater than 1.0f, the value wraps around starting from 0.0f.
// result = (c1 * s) + c2
void colorScaleAddWrap(glm::vec4& result, const glm::vec4& color1, float s, const glm::vec4& color2);

// Modulate and Add  R,G,B,A  componets.  Does not check if values are greater than 1.0f.
// result = (c1 * c2) + c3
void colorModulateAddOvershoot(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, const glm::vec4& color3);
// Modulate and Add  R,G,B,A  componets.  Clamp at 1.0f.
// result = (c1 * c2) + c3
void colorModulateAddClamp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, const glm::vec4& color3);
// Modulate and Add  R,G,B,A  componets.  If value is greater than 1.0f, the value wraps around starting from 0.0f.
// result = (c1 * c2) + c3
void colorModulateAddWrap(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, const glm::vec4& color3);

// Linear interpolation of r,g,b, and a. color1 + s(color2-color1)
void colorLerp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, float s);

// Smooth 'ess-shape' or 'S-shape' interpolation of R,G,B,A components.
// Hermite interpolation between position 1 (when s == 0) and position 2 (when s == 1),
// with implicit tangents that run parallel to the time axis (horizontal time line).
void colorEase(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, float s);

// HSL colorspace interpolate colors.
// Convert RBG to HSV, interpolate HSV components and Alpha, convert result HSV to RGB.
void colorHSVLerp(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, float s);

// HSL colorspace interpolate colors.
// Convert RBG to HSV, interpolate HSV components and Alpha, convert result HSV to RGB.
void colorHSVEase(glm::vec4& result, const glm::vec4& color1, const glm::vec4& color2, float s);

// Interpolate r,g,b between desaturated color and color.
// DesaturatedColor + s(Color - DesaturatedColor)
void colorAdjustSaturation(glm::vec4& result, const glm::vec4& color, float s);

// Interpolate r,g,b between 50% grey and color.  Grey + s(Color - Grey)
void colorAdjustContrast(glm::vec4& result, const glm::vec4& color, float c);

// HSL colorspace conversion
// Convert to Red, Green, Blue from Hue, Saturation, Value.
//  Red, Green, Blue are normalized between 0 and 1.
//  Hue is an angle between 0 and 360.
//  Saturation is between 0 and 1, 0 = no saturation (or white), 1 = completly saturated.
//  Value is between 0 and 1, 0 = no brightness (or black), 1 = bright.
void colorHSVtoRGB(float& r, float& g, float& b, float h, float s, float v);

// Convert to Hue, Saturation, Value from Red, Green Blue.
//  Red, Green, Blue are normalized between 0 and 1.
//  Hue is an angle between 0 and 360.
//  Saturation is between 0 and 1, 0 = no saturation (or white), 1 = completly saturated.
//  Value is between 0 and 1, 0 = no brightness (or black), 1 = bright.
void colorRGBtoHSV(float& h, float& s, float& v, float r, float g, float b);

} // namespace glc
