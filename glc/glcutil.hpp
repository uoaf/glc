#pragma once
#include "glminc.hpp"
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <cmath>

#ifndef ASSERT
#define ASSERT assert
#endif

namespace glc {

// Scalar Relational
template <typename genType>
constexpr bool equal(const genType a, const genType b, const genType epsilon) {
  genType tmp = a - b;
  return ((tmp >= -epsilon) && (tmp <= epsilon));
}

template <typename genType>
constexpr bool notEqual(const genType a, const genType b, const genType epsilon) {
  genType tmp = a - b;
  return ((tmp < -epsilon) || (tmp > epsilon));
}

template <typename genType>
constexpr bool isOne(const genType a, const genType epsilon) {
  genType tmp = a - 1.0f;
  return ((tmp >= -epsilon) && (tmp <= epsilon));
}

template <typename genType>
constexpr bool nonZero(const genType a, const genType epsilon) {
  return ((a < -epsilon) || (a > epsilon));
}

template <typename genType>
constexpr bool isZero(const genType a, const genType epsilon) {
  return ((a >= -epsilon) && (a <= epsilon));
}

// Clamp a value to an upper bound e.g. value = clampHigh( value, 1.0f );
template <typename genType>
genType clampHigh(genType value, genType maxvalue) {
  return value > maxvalue ? maxvalue : value;
}

// Clamp a value to a lower bound e.g. value = clampLow( value, 0.0f );
template <typename genType>
genType clampLow(genType value, genType minvalue) {
  return value < minvalue ? minvalue : value;
}

// Clamp a value within the range 0.0 to 1.0. e.g. value = clampUnit( value );
template <typename genType>
genType clampUnit(genType value) {
  return glm::clamp(value, 0.0f, 1.0f);
}

// Wrap unit value between 0.0f and 1.0f
template <typename genType>
genType wrapUnit(genType unitvalue) {
  if(unitvalue > 1.0f)
    unitvalue -= 1.0f;
  if(unitvalue < 0.0f)
    unitvalue += 1.0f;
  return unitvalue;
}

// Linear Interpolation, t is a percentage value between 0 and 1.
// This function linearly scales between a and b.
template <typename genType>
genType lerp(genType a, genType b, genType t) {
  return (a + (t * (b - a)));
}

} // namespace glc
