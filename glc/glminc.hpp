#pragma once
// clang-format off
#define GLM_FORCE_PURE
#include <glm/glm.hpp>
#include <glm/ext/scalar_common.hpp>
#include <glm/ext/scalar_constants.hpp>
#include <glm/ext/scalar_relational.hpp>
// clang-format on
