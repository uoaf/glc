# glc
### Color library for 3D graphics. Complementary to using glm.


## Usage
This library can be side-by-side with glm. Use this the same way you use glm.

## Roadmap
Support for 3D rendering graphics demos.

## Contributing
Open to contributions. Fork, branch, create pull request.  

## License
Apache 2.0.   



